# tPAF_Vilstrup_2024

This repository contains analysis scripts and processed data formats relating to the paper
Vilstrup et al., 2024 "A germline PAF1 paralog complex ensures cell type-specific gene expression"

